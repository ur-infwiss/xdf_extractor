import argparse
import os
import struct
import wave
from pyxdf import load_xdf

parser = argparse.ArgumentParser(description='Extracts all audio recordings from a xdf file.')
parser.add_argument('filepath', type=str, help='The path to the xdf[z] file')
parser.add_argument('--wave-postfix', '-wpx', dest='wave_postfix', type=str, default='',
                    help="A postfix added to all extracted files")
parser.add_argument('-f', '--force', action='store_true', dest='overwrite_file',
                    help='Forces the destfile to override existing files. BE CAREFUL!!!')

args = parser.parse_args()
xdf_location = args.filepath
wave_postfix = args.wave_postfix
overwrite_file = args.overwrite_file

print("Loading", xdf_location)

streams, fileInfo = load_xdf(xdf_location)

print("Processing streams")
for stream in streams:
    stream_info = stream['info']
    if stream_info['type'][0].lower() == "Audio".lower():
        stream_name = stream_info['name'][0]
        filename = stream_name + wave_postfix + ".wav"
        print("Writing stream", stream_name, "to", filename)

        if not overwrite_file and os.path.exists(filename):
            print('The destfile \'' + filename + '\' already exists and must not be overwritten. '
                                                 'Choose another postfix or set the --force flag.')
            continue

        with wave.open(filename, mode='wb') as wave_file:
            channel_count = int(stream_info['channel_count'][0])
            if channel_count > 2:
                print("Warning: Only stereo files can be created. However, stream",
                      stream_name, "has", channel_count, "channels. Only the first two channels will be extracted")
                channel_count = 2
            wave_file.setnchannels(channel_count)

            wave_file.setframerate(float(stream_info['nominal_srate'][0]))

            channel_format = stream_info['channel_format'][0]
            floating_channel_to_int_multiplier = None
            struct_format = None
            if channel_format == 'int8':
                wave_file.setsampwidth(1)  # samplewith is in bytes
                struct_format = '<b'  # signed char, little endian, see https://docs.python.org/2/library/struct.html#format-characters
            elif channel_format == 'int16':
                wave_file.setsampwidth(2)
                struct_format = '<h'  # signed short, little endian
            elif channel_format == 'int32':
                wave_file.setsampwidth(4)
                struct_format = '<i'  # signed int, little endian
            elif channel_format == 'int64':
                # more than 32 bit precision is not allowed, see https://docs.python.org/2/library/audioop.html
                wave_file.setsampwidth(4)
                struct_format = '<i'  # signed int, little endian
                print("Warning: Stream", stream_name,
                      "uses the int64 format but wave files can \"only\" be in int32 format.")
            elif channel_format in ['float32', 'double64']:
                wave_file.setsampwidth(2)
                struct_format = '<h'  # signed short, little endian
                floating_channel_to_int_multiplier = 65536
            else:
                raise ValueError("Error: Audiostream ", stream_name, "is coded with the unsuported channel_format",
                                 channel_format,
                                 "and has to be skipped.")

            samples = stream['time_series']
            wave_file.setnframes(len(samples))
            for sample in samples:
                for channel_value in sample:
                    if floating_channel_to_int_multiplier:
                        channel_value = int(channel_value * floating_channel_to_int_multiplier)
                    else:
                        channel_value = int(channel_value) # load_xdf always returns numpy floats

                    # https://www.tutorialspoint.com/read-and-write-wav-files-using-python-wave
                    data = struct.pack(struct_format, channel_value)
                    wave_file.writeframesraw(data)

print("finished")
